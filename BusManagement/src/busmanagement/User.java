
package busmanagement;


public class User {
    
   public String Firstname;
   public String Lastname;
   public String Password;
   public String Username;
    
    
    User()
    {
        
    }
    public void setFirstname(String Fname)
    {
        this.Firstname=Fname;
    }
    public String getFirstname()
    {
        return Firstname;
    }
    public void setLastname(String Lname)
    {
        this.Lastname=Lname;
    }
    public String getLastname()
    {
        return Lastname;
    }
    public void setPassword(String Pass)
    {
        this.Password=Pass;
    }
    public String getPassword()
    {
        return Password;
    }
     public void setUsername(String usname)
    {
        this.Username=usname;
    } 
     public String getUsername()
     {
         return Username;
     }
}
