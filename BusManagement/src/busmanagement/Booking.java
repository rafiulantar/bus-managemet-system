package busmanagement;


import static busmanagement.GetAllInfo.userAr;
import javax.swing.*;
import java.time.LocalDate;
import java.awt.BorderLayout;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.ButtonGroup;
import java.awt.event.ActionListener;
import java.awt.event.*;
import java.sql.ResultSet;
import java.sql.*;
import java.awt.*;
import java.time.LocalDate;

class Booking extends JFrame implements ActionListener
{
    
	JButton seates [] = new JButton[40];
	JButton reserve;
	int seatesBooked [] = new int[40];   
	JLabel lblName1, lblName2, lblName3, lblName4;
	JTextField txtfield1;
	//String Busname[]={"","JR","GreenLine","Royal"};
	//JComboBox <String> comboBox =new JComboBox<>(Busname);
	String PassengerName;
	Bus bus;
	
	int cnt=0;
	int x=50,y=100,i=1;
    public Booking(String PassengerName,Bus bus)
    {
		//System.out.println(bus.CompanyName);
		
		super("Welcome "+ PassengerName);
               // System.out.println(bus.CompanyName);
		this.bus=bus;
		this.PassengerName = PassengerName;
		
		  Connection con=null;
                  String sql="SELECT * FROM `booking_details`";
                  ResultSet rs;
                   Statement stmt;
             try
             {
                  con=GetConnection.getconn();
		
                  stmt=con.createStatement();  
                 rs=stmt.executeQuery("select * from booking_details");
                while(rs.next())
                {
                  String Companyname=new String(rs.getString(1));
                   String numberplate=new String(rs.getString(2));
                    String Date=new String(rs.getString(6));
                    if(Companyname.equals(bus.CompanyName)&&numberplate.equals(bus.NumberPlate)&&Date.equals(bus.Date))
                    {
                        
                        seatesBooked [rs.getInt(3)-1]=1;
                        
                        
                        
                    }
                
               
                
                  }
                rs.close();
                stmt.close();
                con.close();
                
                
             }
       catch(Exception e)
               {
               
               
               }

		while(i<=40)
		{				
			seates[cnt]=new JButton(String.valueOf(i));
			seates[cnt].setBounds(x,y,50,50);
			seates[cnt].setOpaque(true);;
			
			
			
			if(seatesBooked [cnt] ==0)
			{
				seates[cnt].setBackground(Color.GREEN);
			}
			else
			{
				seates[cnt].setBackground(Color.RED);
			}
			
            seates[cnt].setForeground(Color.GRAY);	
			add(seates[cnt]);
			
			x+=70;
			if(i%10==0)
			{
				y+=100;
				x=50;
			}
			i++;
			cnt++;
			
		}
		
		
		lblName1=new JLabel("Red Color means seat is Booked already");
		lblName1.setBounds(50,30,500,20);
		lblName1.setFont(new Font("Courier New", Font.BOLD,15));
		add(lblName1);
		
		lblName2=new JLabel("Green Color means seat is not Booked");
		lblName2.setBounds(500,30,500,20);
		lblName2.setFont(new Font("Courier New", Font.BOLD,15));
		add(lblName2);
		
		
		lblName3=new JLabel("Enter seat number to book");
		lblName3.setBounds(500,500,500,20);
		lblName3.setFont(new Font("Courier New", Font.BOLD,15));
		add(lblName3);
		
		
		lblName4=new JLabel("Select Your preferred Bus Service");
		lblName4.setBounds(0,500,500,20);
		lblName4.setFont(new Font("Courier New", Font.BOLD,15));
		add(lblName4);
		
		txtfield1 =new JTextField();
		txtfield1.setBounds(750,500,100,20);
		add(txtfield1);
		
		
		
		
		//comboBox.addActionListener(this);
		
		
		
		
		reserve =new JButton("reserve");
		reserve.setBounds(400,550,100,50);
		add(reserve);
		reserve.addActionListener(this);
		

        this.setSize(1000,1000);
        this.setLayout(null);
        this.setVisible(true);
		
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }
	
	
	
	

	
	public void actionPerformed(ActionEvent event)
	{
		if(event.getSource()== reserve)
		{
			try
			{
				
				//LocalDate date = LocalDate.now();
				String today =bus.Date;
				
				String s = txtfield1.getText().toString();
				int seatNO = Integer.parseInt(s);
				
				if(seatNO>40 || seatNO<1 ) JOptionPane.showMessageDialog(this,"Input valid seat Number");
				else if(seatesBooked [seatNO-1]==1) JOptionPane.showMessageDialog(this,"This seat is already booked ");
				else
				{
					int input = JOptionPane.showConfirmDialog(null, "Are you sure you wanna buy tis ticket\nand chosen seat number is correct?");
				
					if(input==0)
					{	
							
						JOptionPane.showMessageDialog(this,"Successfully done.\nThe amount has been deducted from your Account");
						seates[seatNO-1].setBackground(Color.RED);	
						seatesBooked [seatNO-1] =1;
						txtfield1.setText("");
						//String Busname=comboBox.getSelectedItem().toString();
						
					
						Connection con=GetConnection.getconn();

						//Statement stmt=con.createStatement();  
 String query = "INSERT INTO `booking_details` (`companyname`, `numberplate`, `seat`, `Route`, `User`, `Date`) VALUES (?,?,?,?,? ,?)";         
						//stmt.executeUpdate(query);
						
                                                 PreparedStatement preparedStmt = con.prepareStatement(query);
                                                  preparedStmt.setString (1,bus.CompanyName  );
                                            preparedStmt.setString (2, bus.NumberPlate );
                                                preparedStmt.setInt   (3, seatNO);
                                                    preparedStmt.setString(4,bus.destination);
                                                  // preparedStmt.setInt    (5, 5000);
                                                      preparedStmt.setString (5,this.PassengerName  );
                                                         preparedStmt.setString (6,bus.Date  );

      // execute the preparedstatement
                                                            preparedStmt.execute();
                                                            System.out.println("Shuvo");
						
						Object[] options = {"Print Ticket"};
						JOptionPane.showOptionDialog(null, "Date : "+today+"\n"+"Passenger Name : "+PassengerName+"\n"+"Seat Number : "+ seatNO, "Ticket",JOptionPane.OK_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, null);
						
						con.close(); 
				
					}				
					else
					{
						JOptionPane.showMessageDialog(this,"Booked seat has been canceled");
						txtfield1.setText("");
					}
				}
				
				
			}
			catch(Exception e){JOptionPane.showMessageDialog(this,"no input or problem in query"); txtfield1.setText("");} 
		}
		
		
		
		
		
	}
	
    
}
