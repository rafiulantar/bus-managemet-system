
package busmanagement;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.JComboBox;


public class GetAllInfo {
   static ArrayList<User> userAr = new ArrayList<User>();
   static ArrayList<Bus> busAr = new ArrayList<Bus>();
   public static  User user;
   static void setuser() throws SQLException
   {
       Connection con=null;
             String sql="SELECT * FROM `user_details`";
             ResultSet rs;
            Statement stmt;
             try
             {
                  con=GetConnection.getconn();
		
       stmt=con.createStatement();  
                 rs=stmt.executeQuery("select * from user_details");
                while(rs.next())
                {
                User user=new User();
                user.Firstname=rs.getString(1);
                user.Lastname=rs.getString(2);
                user.Username=rs.getString(3);
                user.Password=rs.getString(4).toString();
               // System.out.println(user.Password);
                userAr.add(user);
               
                
                  }
                rs.close();
                stmt.close();
                con.close();
                
                
             }
       catch(Exception e)
               {
               
               
               }
             
       
   }
   public static ArrayList<User> getuser()
   {
       return userAr;
       
   }
   public static void setBus() throws SQLException
   {
       Connection con=null;
       //System.out.println("something");
            // String sql="SELECT * FROM `bus_details`";
            ResultSet rs;
            Statement stmt;
             try
             {
                // System.out.println("something");
                  con=GetConnection.getconn();
                  //System.out.println("somethingpp");
		stmt=con.createStatement(); 
                //System.out.println("somethingppp");
                rs=stmt.executeQuery("SELECT * FROM `bus_details`");
               // System.out.println("somethingpppppp");
               // System.out.println(rs.getString(1));

                while(rs.next())
                {
                Bus b=new Bus();
                b.CompanyName=rs.getString(1);
                b.NumberPlate=rs.getString(2);
                b.Seat=rs.getInt(3);
                b.Price=rs.getDouble(4);
                b.destination=rs.getString(5);
               b.PickupPoint=rs.getString(6);
               b.RouteTime=rs.getString(7);
               b.Date=rs.getString(8);               // System.out.println(user.Password);
                busAr.add(b);
                System.out.println(b.CompanyName);
               
                  }
                rs.close();
                stmt.close();
                con.close();
                
                
                
             }
       catch(Exception e)
               {
               System.out.println(e+"pp");
               
               }
          
   }

    /**
     *
     * @return
     */
    public static ArrayList<Bus> getBus()
           {
              // Collections.sort(busAr);
               return busAr;
           }
    public static void UpdateUser()
    {
         Connection con;
         Statement stmt = null;
         ResultSet rs;
        
        ArrayList <User> user=GetAllInfo.getuser();
       
        try{  
		 
		con=GetConnection.getconn();
                for(User user1:user){
		 stmt=con.createStatement();  
		  String Query="UPDATE `user_details` SET `firstname`=?,`lastname`=?,`username`=?,`password`=? where `username`=? ";
                PreparedStatement preparedStmt = con.prepareStatement(Query);
      preparedStmt.setString   (1, user1.Firstname);  
      preparedStmt.setString   (2, user1.Lastname);
      preparedStmt.setString   (3,user1.Username);
      preparedStmt.setString   (4, user1.Password);
      preparedStmt.setString   (5, user1.Username);
      preparedStmt.executeUpdate();
      
                }
                 stmt.close();
		con.close(); 
                
             
               
	}catch(Exception e){ System.out.println(e.getMessage());}  

    }
     public static void UpdateBus()
    {
         Connection con;
         Statement stmt = null;
         ResultSet rs;
        
        ArrayList <Bus> busAr=GetAllInfo.getBus();
       
        try{  
		 
		con=GetConnection.getconn();
                for(Bus b:busAr){
		 stmt=con.createStatement(); 
   String Query="UPDATE `bus_details` SET `companyname`=?,`numberplate`=?,`seat`=?,`price`=?,`destination`=?,`PickUpPoint`=?,`routetime`=?,`date`=?";
        PreparedStatement preparedStmt = con.prepareStatement(Query);
      preparedStmt.setString   (1, b.CompanyName);  
      preparedStmt.setString   (2, b.NumberPlate);
      preparedStmt.setInt   (3,b.Seat);
      preparedStmt.setDouble   (4, 600);
      preparedStmt.setString   (5, b.destination);
     preparedStmt.setString   (6, b.PickupPoint);
         preparedStmt.setString   (7, b.RouteTime);
          preparedStmt.setString   (8, b.Date);
          
      
      preparedStmt.executeUpdate();
      
                }
                 stmt.close();
		con.close(); 
                
             
               
	}catch(Exception e){ System.out.println(e.getMessage());}  

    }
}
  
  
